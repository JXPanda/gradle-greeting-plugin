# gradle-greeting-plugin

#### 项目介绍
gradle插件测试

#### 安装教程

```groovy
plugins {
  id "jxpanda.greeting" version "1.0.2"
}
```

#### 使用说明
1. 没什么特别说明的，就是个demo，学习如何开发gradle的插件
2. [从gradle的官方文档抄过来的](https://guides.gradle.org/publishing-plugins-to-gradle-plugin-portal/)

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)