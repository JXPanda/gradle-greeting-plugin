package com.jxpanda.greeting

import org.gradle.api.Plugin
import org.gradle.api.Project

class GreetingPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.tasks.create("hello", Greeting::class.java) { // <1>
            task ->
            task.message = "Hello"
            task.recipient = "Panda"                                // <2>
        }
    }
}
