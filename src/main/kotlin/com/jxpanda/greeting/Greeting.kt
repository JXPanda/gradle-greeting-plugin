package com.jxpanda.greeting

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

open class Greeting : DefaultTask() { // <1>
    var message: String? = null
    var recipient: String? = null

    @TaskAction
    internal fun sayGreeting() {
        println("$message, $recipient") // <2>
    }
}
